﻿namespace body
{
    public class Head
    {
        Ear leftEar;
        Ear rightEar;
        Beard beard;
        List<Hair> hairList;
        Eye rightEye;
        Eye leftEye;
        Nose nose;
        Mouth mouth; 
        public Head()
        {
            leftEar = new Ear();
            rightEar = new Ear();
            beard = new Beard();
            rightEye = new Eye();
            leftEye = new Eye();
            nose = new Nose();
            mouth = new Mouth();
            Hair hair = new Hair();
            hairList = new List<Hair>(){hair};
        }
    }
}